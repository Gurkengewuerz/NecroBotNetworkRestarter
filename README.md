NecroBot Network Updater
--------------
The Updater updates one botting-"network"

Example Config File (*config.cfg*):

    ptc:bot1:pw1:null
    google:bot3@gmail.com:pw3:null
    google:bot4@gmail.com:pw4:null:"C:\Configs\SpecialConfig.json"
    ptc:bot5:pw5:0

Syntax:
authtype:username:password:proxy.cfg Index[:CustomConfig]


Example Proxy Config (*proxy.cfg*):

    169.57.157.148:8080
    169.57.157.148:8080:user:password
    169.57.157.148:8080:dudu
   
Syntax:
host:port[:username:password]


The program needs one argument: Default Config

**Windows:**
Therefor create a shortcut and add the minutes at the end like

    "Z:\C#\NecroBotNetworkRestarter\NecroBotNetworkRestarter\bin\Debug\NecroBotNetworkRestarter.exe" "Z:\C#\NecroBotNetworkRestarter\NecroBotNetworkRestarter\bin\Debug\Configs\default.json"

RUN THE SHORTCUT AS ADMIN FOR THE SYM LINK!



Website: [Gurkengewuerz](https://gurkengewuerz.de/web)

Twitter: [Gurkengewuerz](https://twitter.com/Gurkengewuerz)