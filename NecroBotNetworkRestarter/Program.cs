﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using RestSharp;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace NecroBotNetworkUpdater
{
	class MainClass
	{
		[DllImport("Kernel32")]
		private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

		private delegate bool EventHandler(CtrlType sig);
		static EventHandler _handler;

		enum CtrlType
		{
			CTRL_C_EVENT = 0,
			CTRL_BREAK_EVENT = 1,
			CTRL_CLOSE_EVENT = 2,
			CTRL_LOGOFF_EVENT = 5,
			CTRL_SHUTDOWN_EVENT = 6
		}

		private static bool Handler(CtrlType sig)
		{
			switch (sig)
			{
				case CtrlType.CTRL_C_EVENT:
				case CtrlType.CTRL_LOGOFF_EVENT:
				case CtrlType.CTRL_SHUTDOWN_EVENT:
				case CtrlType.CTRL_CLOSE_EVENT:
					stop();
					return false;
				default:
					return false;
			}
		}

		static string exePath = "";
		public static void Main(string[] args)
		{
			if (args.Length != 1)
			{
				Logger.error("Syntax: <BotConfig.json>");
				Console.ReadLine();
				return;
			}

			string globalConfig = args[0];
			if (!File.Exists(globalConfig))
			{
				Logger.error(globalConfig + " must Exists!");
				Console.ReadLine();
				return;
			}

			_handler += Handler;
			SetConsoleCtrlHandler(_handler, true);

			exePath = AppDomain.CurrentDomain.BaseDirectory;
			if (File.Exists(Path.Combine(exePath, "config.cfg")))
			{
				string APIurl = "https://api.github.com/repos/NECROBOTIO/NecroBot/releases";

				var client = new RestClient();
				client.BaseUrl = new Uri(APIurl);
				IRestResponse response = client.Execute(new RestRequest());
				if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
				{
					string responseStr = response.Content;
					List<GitHubJSON.RootObject> root = JsonConvert.DeserializeObject<List<GitHubJSON.RootObject>>(responseStr);
					string name = root[0].name;
					string dwLink = root[0].assets[0].browser_download_url;
					string body = root[0].body;

					string tmpFolder = Path.Combine(exePath, "tmp");
					if (!Directory.Exists(tmpFolder))
					{
						Directory.CreateDirectory(tmpFolder);
						Logger.debug("creating ./tmp Folder");
					}

					Logger.log("Downloading " + name);
					Logger.log("- - - GitHub Release Infos - - -");
					Logger.log(body);
					Logger.log("- - - - - - - - - - - - - - -");

					string botFile = Path.Combine(tmpFolder, "bot.zip");
					client.BaseUrl = new Uri(dwLink);
					File.WriteAllBytes(botFile, client.DownloadData(new RestRequest()));

					Logger.debug("extract NecroBot ZipBall");
					string releaseFolder = Path.Combine(tmpFolder, "Release");
					FileUtils.ExtractZipFile(botFile, null, releaseFolder);
					if (Directory.Exists(Path.Combine(releaseFolder, "Release")))
					{
						FileUtils.CopyFolderContents(Path.Combine(releaseFolder, "Release"), releaseFolder);
						Directory.Delete(Path.Combine(releaseFolder, "Release"), true);
					}

					string netFolder = Path.Combine(exePath, "Network");
					if (!Directory.Exists(netFolder))
					{
						Directory.CreateDirectory(netFolder);
						Logger.debug("creating Network folder");
					}

					Logger.debug("reading config.cfg");
					string[] accsandPws = File.ReadAllLines(Path.Combine(exePath, "config.cfg"));
					foreach (string str in accsandPws)
					{
						string[] accPW = str.Split(':');
						if (accPW.Length >= 3)
						{
							// authtype:username:password:googlerefreshtoken[:CustomConfig]
							string authType = accPW[0];
							string acc = accPW[1];
							string pw = accPW[2];

							int proxyIndex = -1;
							bool useProxy = false;
							string[] proxyInfo = { null, null, null, null };
							if (accPW.Length >= 4)
							{
								try
								{
									if (!accPW[3].ToLower().Equals("null"))
									{
										proxyIndex = Convert.ToInt32(accPW[3]);
										if (getProxy(proxyIndex) != null)
										{
											proxyInfo = getProxy(proxyIndex);
											useProxy = true;
											Logger.debug("Using Proxy: " + string.Join(",", proxyInfo));
										}
									}
								}
								catch (Exception ex)
								{
									Logger.printStackTrace(ex);
								}
							}

							Logger.log("Creating Bot " + acc);

							string botFolder = Path.Combine(netFolder, acc);
							string botConfigFolder = Path.Combine(botFolder, "config");
							string configJSON = Path.Combine(botConfigFolder, "config.json");
							string authJSON = Path.Combine(botConfigFolder, "auth.json");

							if (Directory.Exists(botFolder))
							{
								Logger.error("    skipping folder " + acc);
								continue;
							}
							Directory.CreateDirectory(botFolder);
							FileUtils.CopyFolderContents(releaseFolder, botFolder);
							JObject rss =
												new JObject(
															new JProperty("AuthType", authType),
															new JProperty("PtcUsername", (authType == "ptc") ? acc : null),
															new JProperty("PtcPassword", (authType == "ptc") ? pw : null),
															new JProperty("GoogleUsername", (authType == "google") ? acc : null),
															new JProperty("GooglePassword", (authType == "google") ? pw : null),
															new JProperty("UseProxy", useProxy),
															new JProperty("UseProxyHost", useProxy ? proxyInfo[0] : null),
															new JProperty("UseProxyPort", useProxy ? proxyInfo[1] : null),
															new JProperty("UseProxyAuthentication", (proxyInfo[2] != null)),
															new JProperty("UseProxyUsername", useProxy ? proxyInfo[2] : null),
															new JProperty("UseProxyPassword", useProxy ? proxyInfo[3] : null),
															new JProperty("DevicePackageName", "random"),
															new JProperty("DeviceId", "8525f5d8201f78b5"),
															new JProperty("AndroidBoardName", "msm8996"),
															new JProperty("AndroidBootloader", "1.0.0.0000"),
															new JProperty("DeviceBrand", "HTC"),
															new JProperty("DeviceModel", "HTC 10"),
															new JProperty("DeviceModelIdentifier", "pmewl_00531"),
															new JProperty("DeviceModelBoot", "qcom"),
															new JProperty("HardwareManufacturer", "HTC"),
															new JProperty("HardwareModel", "HTC 10"),
															new JProperty("FirmwareBrand", "pmewl_00531"),
															new JProperty("FirmwareTags", "release-keys"),
															new JProperty("FirmwareType", "user"),
															new JProperty("FirmwareFingerprint", "htc/pmewl_00531/htc_pmewl:6.0.1/MMB29M/770927.1:user/release-keys")

								);
							Logger.debug("    Write auth.json");
							File.WriteAllText(authJSON, rss.ToString());

							Logger.debug("    creating sym. Link");
							if (accPW.Length >= 5)
							{
								string altconfig = "";
								for (int i = 4; i < accPW.Length; i++)
								{
									altconfig += accPW[i];
									if (i < accPW.Length - 1)
									{
										altconfig += ":";
									}
								}
								if (File.Exists(altconfig.Replace("\"", "")))
								{
									runCMD("mklink \"" + configJSON + "\" " + altconfig);
								}
								else
								{
									Logger.error("    FAILED TO LOAD SPECIAL FILE " + altconfig);
								}
							}
							else
							{
								runCMD("mklink \"" + configJSON + "\" \"" + globalConfig + "\"");
							}

							Logger.log("    " + acc + " finish");
							Logger.log("");
						}
						else
						{
							Logger.error("INVALID LINE: " + str);
						}
					}
				}
				else
				{
					Logger.error("No Response from " + APIurl);
				}
			}
			else
			{
				Logger.error("config.cfg does not exists");
				Logger.log("Syntax: authtype:username:password:googlerefreshtoken[:CustomConfig]");
				generateConfig();
			}

			Logger.log("");
			Logger.log("");
			Logger.log("SUCCESS!");
			stop();
			Console.ReadLine();
		}

		public static void END()
		{
			System.Threading.Thread.Sleep(10 * 1000);
			Environment.Exit(0);
		}

		public static void generateConfig()
		{

		}

		public static void runCMD(string cmnd)
		{
			Process process = new Process();
			process.StartInfo.FileName = "cmd.exe";
			process.StartInfo.Arguments = "/c " + cmnd;
			process.StartInfo.Verb = "runas";
			process.StartInfo.CreateNoWindow = true;
			process.StartInfo.UseShellExecute = false;
			process.StartInfo.RedirectStandardOutput = true;
			process.Start();
		}

		public static void stop()
		{
			string tmpFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tmp");
			if (Directory.Exists(tmpFolder))
			{
				Directory.Delete(tmpFolder, true);
			}
		}

		private static string[] getProxy(int index)
		{
			if (File.Exists(Path.Combine(exePath, "proxy.cfg")))
			{
				string[] proxys = File.ReadAllLines(Path.Combine(exePath, "proxy.cfg"));
				if (proxys.Length >= index)
				{
					// ip:port[:username:password]
					string[] proxyInfo = proxys[index].Split(':');
					if (proxyInfo.Length >= 2)
					{
						if (proxyInfo.Length <= 3)
						{
							Array.Resize(ref proxyInfo, 5);
							proxyInfo[2] = null;
							proxyInfo[3] = null;
						}
						return proxyInfo;
					}
					Logger.error("INVALID PROXY LINE: " + index);
				}
			}
			else 
			{
				Logger.error("Reference to proxy, but proxy.cfg not exists");
			}
			return null;
		}
	}
}
