﻿using System;
namespace NecroBotNetworkUpdater
{
	public class Logger
	{
		public static void debug(Object o) 
		{
			Console.ForegroundColor = ConsoleColor.DarkGreen;
			Console.WriteLine(getPrefix() + " " + o);
			Console.ForegroundColor = ConsoleColor.White;
		}

		public static void log(Object o)
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine(getPrefix() + " " + o);
			Console.ForegroundColor = ConsoleColor.White;
		}

		public static void error(Object o) 
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine(getPrefix() + " " + o);
			Console.ForegroundColor = ConsoleColor.White;
		}

		public static void printStackTrace(Object o) 
		{
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine(getPrefix() + " " + o);
			Console.ForegroundColor = ConsoleColor.White;
		}

		public static String getPrefix()
		{
			return DateTime.Now.ToString("MM-dd-yyyy @ HH:mm:ss");
		}
	}
}

